#include <iostream>
#include "tipo.h"
#include "alberi.h"
#include "coda-bfs.h"
using namespace std;
node* new_node(tipo_inf i)
{
	node* p = new node;
	p->first_child = NULL;
	p->next_sibling = NULL;
	p->parent = NULL;
	copy(p->inf , i);
	return p;
}
void insert_child(tree p , tree c)
{
	// c diventa figlio di p
	c->next_sibling = p->first_child;
	p->first_child = c;
	c->parent = p;

}
void insert_sibling(node* n , tree t)
{
	// t diventa il fratello di n
	t->next_sibling = n->next_sibling;
	n->next_sibling = t;
	t->parent = n->parent;
}
tipo_inf get_info(node* n)
{
	return n->inf;
}
node* get_parent(node* n)
{
	return n->parent;
}
node* get_firstChild(node* n)
{
	return n->first_child;
}
node* get_nextSibling(node* n)
{
	return n->next_sibling;
}
void serialize(tree radice)
{
	cout<<"(";
	print(get_info(radice));
	tree t1 = get_firstChild(radice);
	while(t1 != NULL)
	{
		serialize(t1);
		t1 = get_nextSibling(t1);
	}
	cout<<")";
}

int altezza(tree radice)
{
	if(get_firstChild(radice)== NULL)
		return 0;
	int max = 0;
	int local = 0;
	tree t1 = get_firstChild(radice);
	while(t1 != NULL)
	{
		local = altezza(t1);
		if (local > max)
			max = local;
		t1 = get_nextSibling(t1);
	}
	return max+1;
}
void stampa_DFS(tree radice)
{
	if(radice == NULL)
		return;
	print(get_info(radice));
	cout<<endl;
	tree t1 = get_firstChild(radice);
	while(t1 != NULL)
	{
		stampa_DFS(t1);
		t1 = get_nextSibling(t1);
	}
}
void stampa_BFS(tree radice)
{
	codaBFS Q = newQueue();
	Q = enqueue(Q , radice);
	while(!isEmpty(Q))
	{
		tree t1 = dequeue(Q);
		print(get_info(t1));
		cout<<" ";
		t1 = get_firstChild(t1);
		while(t1 != NULL)
		{
			Q = enqueue(Q , t1);
			t1 = get_nextSibling(t1);
		}
	}
}
bool path(node* n , tipo_inf target)
{
	//ritorna true se esiste un cammino da n a target
	if(n == NULL)
		return false;
	if(compare(n->inf , target) == 0)
		return true;
	tree t1 = get_firstChild(n);
	bool temp;
	bool ris = false;
	while(t1 != NULL && !ris)
	{
		temp = path(t1, target);
		ris = ris || temp;
		t1 = get_nextSibling(t1);
	}
	return ris;
}

int larghezza(tree radice)
{
	codaBFS Q = newQueue();
	int max = 0;
	Q = enqueue(Q , radice);
	while(!isEmpty(Q))
	{
		tree t1 = dequeue(Q);
		t1 = get_firstChild(t1);
		int counter = 0;
		while(t1 != NULL)
		{
			counter++;
			Q = enqueue(Q , t1);
			t1 = get_nextSibling(t1);
		}
		if(max < counter)
			max = counter;
	}
	return max;
}





